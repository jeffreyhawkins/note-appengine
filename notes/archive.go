package notes

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/user"
)

//
// Get all notes
//
func getAllArchivesHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Getting all archives...")

	u := user.Current(c)
	log.Infof(c, "user id: %v", u.ID)
	log.Infof(c, "user email: %v", u.Email)

	q := datastore.NewQuery("Archive").Filter("UserId =", u.ID).Order("Title")
	var notes []Note
	keys, err := q.GetAll(c, &notes)
	if err != nil {
		log.Errorf(c, "fetching archives: %v", err)
		http.Error(w, "Failed gettng archives from datastore.", http.StatusInternalServerError)
		return
	}

	for i, key := range keys {
		notes[i].Id = key.Encode()
	}

	b, e := json.Marshal(notes)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

//
// Get a note by id
//
func getArchiveHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Getting a archive...")

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No id specified in request", http.StatusBadRequest)
		return
	}

	k, e := datastore.DecodeKey(id)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	note := new(Note)
	if err := datastore.Get(c, k, note); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	note.Id = k.Encode()

	b, e := json.Marshal(note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

func moveArchiveHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Moving a archive...")

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No id specified in request", http.StatusBadRequest)
		return
	}

	k, e := datastore.DecodeKey(id)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	note := new(Note)
	e = datastore.Get(c, k, note)
	if e != nil {
		http.Error(w, e.Error(), http.StatusInternalServerError)
		return
	}

	key := datastore.NewIncompleteKey(c, "Note", nil)
	key, e = datastore.Put(c, key, note)
	if e != nil {
		http.Error(w, "Failed to store note.", http.StatusInternalServerError)
		return
	}

	e = datastore.Delete(c, k)
	if e != nil {
		http.Error(w, "Failed delete note from datastore.", http.StatusInternalServerError)
		return
	}
}

func deleteArchiveHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Deleting archive...")

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No id specified in request", http.StatusBadRequest)
		return
	}

	k, e := datastore.DecodeKey(id)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	e = datastore.Delete(c, k)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed delete archive from datastore.", http.StatusInternalServerError)
		return
	}
}
