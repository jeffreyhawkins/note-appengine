package application

import (
	"net/http"

	"bitbucket.org/jeffhawkins/note-appengine/notes"
	"github.com/gorilla/mux"
)

func Run() {
	r := mux.NewRouter()
	notes.SetRoutes(r)
	http.Handle("/", r)
}
