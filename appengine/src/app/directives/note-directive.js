(function () {
  'using strict';

  angular.module('notesApp').directive('note', [
    '$log',
    'noteService',
    function($log, noteService) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.on('click', function(event) {
            //$log.log('You clicked on a note.');
            var value = $(element).find('input').val();
            //$log.log('   value: ' + value);
            //scope.id = value;
            noteService.setSelectedNoteId(value);
          });
        }
      };
    }
  ]);
})();