(function () {
  'using strict';

  angular.module('notesApp').directive('wordCount', [
    '$log',
    function($log) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {

          element.on('keydown', function(e) {
            var text = element.text();
            var divs = element.find('div');
            divs = divs ? divs.length : 0;
            countWords(text, divs);
          });

          var countWords = function(text, divs) {
            if (text) {
              var a = text.match(/\S+/g);
              var len = a.length;

              for (var i = 0; i < a.length; i++) {
                if (a[i] === '-' || a[i] === '*' || a[i] === '&') {
                  len = len - 1;
                }
              } 

              setWordCount(len + divs);
            }
          };

          var init = function() {
            var text = element.text();
            var divs = element.find('div');
            divs = divs ? divs.length : 0;
            countWords(text, divs);
          };

          scope.$watch(function() {
            init();
          });
        
          var setWordCount = function(words) {
            var wc = document.querySelector('.word-count');
            if (wc) {
              if (words === 1) {
                angular.element(wc).text(words + ' word');
              } else {
                angular.element(wc).text(words + ' words');
              }
            }
          };
        }
    };

    }
  ]);
})();