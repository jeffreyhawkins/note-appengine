(function () {
  'using strict';

  angular.module('notesApp').factory('interceptor', [
      '$rootScope',
      function($rootScope) {  
          var interceptor = {
            request: function(config) {
                $rootScope.$broadcast('show-spinner');
                return config;
              },
              response: function(response) {
                $rootScope.$broadcast('hide-spinner');
                  return response;
              }
          };

          return interceptor;
      }
  ]);
})();