(function () {
  'using strict';

  angular.module('notesApp').service('spinService', [
    function() {
        return {
          start: function() {
            $('.fa-save').addClass('fa-spin');
          },
          stop: function() {
            $('.fa-save').removeClass('fa-spin');
          }
      };
    }
  ]);
})();