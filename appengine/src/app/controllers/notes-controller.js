(function () {
  'using strict';

  angular.module('notesApp').controller('NotesController', [
    '$scope',
    '$log',
    '$http',
    '$location',
    function ($scope, $log, $http, $location) {

      $scope.clicked = function(index) {
        //$log.log('index: ' + index);
        $location.path('/edit/' + $scope.notes[index].id);
      };

      $scope.delete = function(index) {
        //var e = angular.element($event.currentTarget);
        //e = e.parent().parent();
        //e.addClass('animated zoomOutUp');
        console.log('index: ' + index);

        var id = $scope.notes[index].id;
        $scope.notes.splice(index, 1);
        $http.delete('/notes/v1/note/' + id).
          then(function(response) {
            //$scope.notes = response.data;
          });
      };

      $scope.init = function() {
        $http.get('/notes/v1/note').
          then(function(response) {
            $scope.notes = response.data;
          });
      };
    }
  ]);
})();