(function () {
  'using strict';

  angular.module('notesApp').controller('ArchivesController', [
    '$scope',
    '$log',
    '$http',
    '$location',
    function ($scope, $log, $http, $location) {

      $scope.clicked = function(index) {
        //$log.log('index: ' + index);
        $location.path('/view/' + $scope.notes[index].id);
      };

      $scope.move = function($event, index) {
        var e = angular.element($event.currentTarget);
        e = e.parent().parent();
        e.addClass('animated fadeOutDown');

        var id = $scope.notes[index].id;
        $scope.notes.splice(index, 1);
        $http.put('/notes/v1/archive/' + id).
          then(function(response) {
            //$scope.notes = response.data;
          });
      };

      $scope.delete = function(index) {
        var id = $scope.notes[index].id;
        $scope.notes.splice(index, 1);
        $http.delete('/notes/v1/archive/' + id).
          then(function(response) {
            //$scope.notes = response.data;
          });
      };

      $scope.init = function() {
        $http.get('/notes/v1/archive').
          then(function(response) {
            $scope.notes = response.data;
          });
      };
    }
  ]);
})();