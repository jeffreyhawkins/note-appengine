(function () {
'using strict';

  angular.module('notesApp').controller('AttachmentsController', [
    '$scope',
    '$log',
    '$http',
    '$location',
    function ($scope, $log, $http, $location) {
      $scope.attachments = [];

      angular.element('#upload-file').change(function() {
        //$log.info("changed...");
        angular.element('#upload-form').submit();
      });

      $scope.init = function() {
        //$log.info("getting all attachments...");

        $http.get('/notes/v1/attachments').
          then(function(response) {
            $scope.attachments = response.data;
          });
      };

      $scope.cancel = function() {
        //$log.log('You clicked on cancel.');
        $location.path('/notes');
      };

      $scope.upload = function() {
        //$log.info('upload...');

        $http.get('/notes/v1/upload/url').
          then(function(response) {
            $scope.url = response.data.url;
            //$log.log('url: ' + JSON.stringify(response.data));
          });

        angular.element('#upload-file').click();
      };

      $scope.delete = function(index) {
        //$log.info('Deleting attachement: ' + index);
        var attachmentId = $scope.attachments[index].blobKey;

        $http.delete('/notes/v1/download/' + attachmentId).
          then(function(response) {
            $scope.attachments.splice(index, 1);
          }); 
      };
    }
  ]);
})();