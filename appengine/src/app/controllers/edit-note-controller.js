(function () {
  'using strict';

  angular.module('notesApp').controller('EditNoteController', [
    '$scope',
    '$log',
    '$location',
    '$http',
    '$routeParams',
    '$timeout',
    'spinService',
    function ($scope, $log, $location, $http, $routeParams, $timeout, spinService) {
      
      $scope.init = function() {
        //$log.log('id: ' + $routeParams.noteId);

        $http.get('/notes/v1/note/' + $routeParams.noteId).
          then(function(response) {
            $scope.note = response.data;
            //$log.info('note: ' + JSON.stringify(response.data));
          });
      };

      $scope.cancel = function() {
        //$log.log('You clicked on cancel.');
        $location.path('/notes');
      };

      $scope.update = function() {
        spinService.start();

        $http.put('/notes/v1/note', $scope.note).
          then(function(response) {
            spinService.stop();
          });
      };
    }
  ]);
})();