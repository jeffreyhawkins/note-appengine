(function () {
  'using strict';

  angular.module('notesApp', ['ngRoute', 'ngAnimate', 'ngCookies']);

  angular.module('notesApp').config([
    '$routeProvider',
    '$compileProvider',
    '$httpProvider',
    function($routeProvider, $compileProvider, $httpProvider) {
      // Performance improvement
      $compileProvider.debugInfoEnabled(false);
      
      $routeProvider.
        when('/add', {
          templateUrl: '/views/add-note.html',
          controller: 'AddNoteController'
        }).
        when('/notes', {
          templateUrl: '/views/notes.html',
          controller: 'NotesController'
        }).
        when('/edit/:noteId', {
          templateUrl: '/views/edit-note.html',
          controller: 'EditNoteController'
        }).
        when('/archives', {
          templateUrl: '/views/archives.html',
          controller: 'ArchivesController'
        }).
        when('/attachments', {
          templateUrl: '/views/attachments.html',
          controller: 'AttachmentsController'
        }).
        otherwise({
          redirectTo: '/notes'
        });
    
        $httpProvider.interceptors.push('interceptor');
  }]);
})();