'use strict';

module.exports = function(config) {
  config.set({
    basePath: '',
    plugins: ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-coverage'],
    frameworks: ['jasmine'],
    files: [
      './node_modules/angular/angular.min.js',
      './node_modules/angular-route/angular-route.min.js',
      './node_modules/angular-cookies/angular-cookies.min.js',
      './node_modules/angular-animate/angular-animate.min.js',
      './node_modules/jquery/dist/jquery.min.js',
      './node_modules/bootstrap/dist/js/bootstrap.min.js',
      './node_modules/moment/min/moment.min.js',
      'src/**/*.js'
    ],
    exclude: [
      'karma.conf.js',
      'Gruntfile.js'
    ],
    coverageReporter: {
      type: 'text'
    },
    preprocessors: {
      'src/**/*-spec.js': ['coverage']
    },
    reporters: ['progress', 'coverage'],
    port: 9876,
    colors: false,
    logLevel: config.DEBUG,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true
  });
};