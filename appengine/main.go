package main

import (
	"bitbucket.org/jeffhawkins/note-appengine/application"
	_ "google.golang.org/appengine/remote_api"
)

func init() {
	application.Run()
}
